# Practica Codificacio Noemi Sauret
## Index
* [Inicialitzar llistes](#item1)
* [Trobar lletres repetides](#item2)
* [Ordenar](#item3)
* [Codificar](#item4)
* [Decodificar](#item5)
* [Execucio](#item6)

<a name="item1"></a>
## Inicialitzar llistes
Inicialitzarem les **llistes** amb un *for* ja que sabem quants comps ha d'entrar a la iteració.

<a name="item2"></a>
## Trobar lletres repetides
Recorrerem el text caràcter per caràcter i mirarem si el caracter en el que estem està a "caracter" si no hi es l'afegirem a la ultima posicio que es on estem, sinó li sumarem una "repeticio" a la posicio on ha trobat el caracter a "caracter".

<a name="item3"></a>
## Ordenar
Ordenarem els caracters segons les seves repeticions amb el metode de la bombolla.

<a name="item4"></a>
## Codificar
Tenim una llista amb tots els codis i com que ja tenim els caracters ordenats per els cops que es repeteixen, recorrerem el text i buscarem en quina posicio de "caracter" esta el caracter en el que estem.
Ara que ja ho sabem sol hem de imprimir el codi en un altre text.

<a name="item5"></a>
## Decodificar
Recorrerem el text on hem posat el text codificat i guardarem els caracters en una variable "coditemp" fin que trobi un **$**, agafarem el resultat de "coditemp" i el comparearem amb la llista de codis, quan coincideixi anirem a "caracter" i l'escriurem en un altre text.

<a name="item6"></a>
## Execucio
![image.png](./image.png)
